var isMobile = navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|Android)/i) != null;
if(isMobile){
    eventpress = 'touchstart';
    eventunpress = 'touchend'
    eventmove = 'touchmove'
} else {
    eventpress = 'mousedown';
    eventunpress = 'mouseup'
    eventmove = 'mousemove'
}

$(function(){

    $(window).resize(function () {
        $("#white-top.margin-bottom").css({"margin-bottom": -($("#fixed-left-menu").height() - 275 + 120)});
        if ($(".product-page").length > 0) {
            $("#white-top").css({"height": $("#product-top-right").height() + 350});
        }

        if ($(".header-full-text").length >0 && $(window).width() > 480){
            var top = $("#content-size").height() - ($(".text-header h1").offset().top + $(".text-header h1").height());
            var marg = (top-$(".header-full-text").height())/2;
            $(".text-header h1").css({"margin-bottom": marg});
            $(".about-content").css({"padding-top":marg});
            $(".text-page-content").css({"padding-top":marg});
            console.log(top);
        }
    });
    $(window).resize();


    $(window).scroll(function () {
        scrollPage();
    });
    $(window).on("mousewheel", function(objEvent, intDelta){
        scrollPage();
    });

    if ($(".big-gallery").length > 0) {
        for (var i=0; i<$(".big-gallery").length; i++ ){
            createBigGallery($(".big-gallery").eq(i));
        }
    }
    if ($(".info-gallery").length > 0) {
        for (var i=0; i<$(".info-gallery").length; i++ ){
            createInfoGallery($(".info-gallery").eq(i));
        }
    }

    function scrollPage() {
        var koef = (37.96 - 17.7)/800;
        var pt = 0;
        if ($(window).width() > 480){
            if ($(".product-page").length > 0 ){
                if ($(window).scrollTop() < 800){
                    pt = $(window).scrollTop();
                    $("#white-top").css({"padding-top": pt});
                    $("body").removeClass("productscroll");
                }else{
                    pt = 800;
                    $("#white-top").css({"padding-top": pt});
                    $("body").addClass("productscroll");
                }
                $(".product-image").width((37.96 - koef * pt) + "%");
            }
        }else{
        }
    }


    var brand_ch = 0;
    $(".product-brands-next").on("click", function () {
        brand_ch++;
        if (brand_ch >= $(".product-brand").length-1){
            $(".product-brands-next").hide();
        }else{
            $(".product-brands-next").show();
        }
        $("#product-brands-list").css({"left": -($(".product-brand").eq(1).position().left)*brand_ch});
    });
    $(".product-brand").on("click", function () {
        brand_ch = $(this).index();
        if (brand_ch >= $(".product-brand").length-1){
            $(".product-brands-next").hide();
        }else{
            $(".product-brands-next").show();
        }
        $("#product-brands-list").css({"left": -($(".product-brand").eq(1).position().left)*brand_ch});
    });

    var br_press_ok = false;
    var br_old_x = 0;
    var br_new_x = 0;
    $(document).on("touchstart", '#product-brands', function(e){
        br_press_ok = true;
        br_old_x = e.originalEvent.touches[0].pageX;
        br_new_x = e.originalEvent.touches[0].pageX;
    });
    $(document).on("touchmove", '#product-brands', function(e){
        if (br_press_ok){
            br_new_x = e.originalEvent.touches[0].pageX;
        }
    });
    $(document).on("touchend", '#product-brands', function(e){
        if (br_press_ok){
            br_press_ok = false;
            if (Math.abs(br_old_x - br_new_x) > 100){
                if (br_old_x < br_new_x){
                    if (brand_ch > 0){
                        $(".product-brand").eq(brand_ch-1).click();
                    }
                } else {
                    if (brand_ch < $(".product-brand").length-1){
                        $(".product-brands-next").click();
                    }
                }
            }
            br_old_x = 0;
            br_new_x = 0;
        }
    });


    $(".form-confirm").on("click", function () {
        $(this).toggleClass("active");
    });
    $("#close-cart").on("click", function () {
        $("#right-cart").removeClass("active");
        $("#all-content").removeClass("fixed");
    });
    $("#close-auth").on("click", function () {
        $("#auth-modal").removeClass("active");
        $("#all-content").removeClass("fixed");
    });
    $("#close-search").on("click", function () {
        $("#search-modal").removeClass("active");
        $("#all-content").removeClass("fixed");
    });

    $("#burger").on("click", function () {
        $(this).toggleClass("active");
        $("#menu-items").toggleClass("active");
        if (!$(this).hasClass("active")){
            $("#all-content").removeClass("fixed");
            $("#catalog-menu").removeClass("active");
            $("#brands-menu").removeClass("active");
        }
    });


    $("#cart-count").on("click", function () {
        $("#right-cart").addClass("active");
        $("#all-content").addClass("fixed");
        return false;
    });
    $("#auth-btn").on("click", function () {
        $("#auth-modal").addClass("active");
        $("#all-content").addClass("fixed");
        return false;
    });
    $("#search-btn").on("click", function () {
        $("#search-modal").addClass("active");
        $("#all-content").addClass("fixed");
        return false;
    });
    $(".brands-menu-btn").on("click", function () {
        $(".catalog-submenu").slideUp(300);
        $("#catalog-menu").removeClass("active");
        $("#brands-menu").addClass("active");
        $("#all-content").addClass("fixed");
        return false;
    });
    $("#close-brands").on("click", function () {
        $("#brands-menu").removeClass("active");
        $("#all-content").removeClass("fixed");
        return false;
    });

    $(".catalog-menu-link").on("click", function () {
        $(".catalog-submenu").slideDown(300);
        $("#brands-menu").removeClass("active");
        $("#catalog-menu").addClass("active");
        $("#all-content").addClass("fixed");
        return false;
    });
    $("#close-catalog-menu").on("click", function () {
        $(".catalog-submenu").slideUp(300);
        $("#catalog-menu").removeClass("active");
        $("#all-content").removeClass("fixed");
        return false;
    });

    if ($("#bestsellers-items").length > 0){
        createBestsellersGallery($("#home-bestsellers"));
    }

});


function createBestsellersGallery(gallery){
    var gal_ch = 0;
    gallery.find(".bestsellers-next").on("click", function () {
        gal_ch++;
        var rast = gallery.find(".product").eq(1).position().left - gallery.find(".product").eq(0).position().left;
        if ($(window).width() <= 1500){
            if (gal_ch >= gallery.find(".product").length-2){
                gallery.find(".bestsellers-next").hide();
            }else{
                gallery.find(".bestsellers-next").show();
            }
        }else{
            if (gal_ch >= gallery.find(".product").length-3){
                gallery.find(".bestsellers-next").hide();
            }else{
                gallery.find(".bestsellers-next").show();
            }
        }
        $("#bestsellers-items").css({"left": -gal_ch*rast});
    });
    gallery.find(".product").on("click", function () {
        gal_ch = $(this).index();
        var rast = gallery.find(".product").eq(1).position().left - gallery.find(".product").eq(0).position().left;
        if ($(window).width() <= 1500){
            if (gal_ch >= gallery.find(".product").length-2){
                gallery.find(".bestsellers-next").hide();
            }else{
                gallery.find(".bestsellers-next").show();
            }
        }else{
            if (gal_ch >= gallery.find(".product").length-3){
                gallery.find(".bestsellers-next").hide();
            }else{
                gallery.find(".bestsellers-next").show();
            }
        }
        $("#bestsellers-items").css({"left": -gal_ch*rast});

    });
}
function createBigGallery(gallery){
    var gal_ch = 0;
    gallery.find(".big-gallery-next").on("click", function () {
        gal_ch++;
        if (gal_ch >= gallery.find(".big-gallery-image").length-1){
            gallery.find(".big-gallery-next").hide();
        }else{
            gallery.find(".big-gallery-next").show();
        }
        if ($(window).width() > 480){
            gallery.find(".big-gallery-images").css({"left": -823*gal_ch});
        }else{
            gallery.find(".big-gallery-images").css({"left": -(gallery.find(".big-gallery-image").width()+15)*gal_ch});
        }
        gallery.find(".big-gallery-image").css({"opacity": 0.1});
        gallery.find(".big-gallery-image").eq(gal_ch).css({"opacity": 1});
        gallery.find(".big-gallery-num b").html("0"+(gal_ch+1));
    });
    gallery.find(".big-gallery-image").on("click", function () {
        gal_ch = $(this).index();
        if (gal_ch >= gallery.find(".big-gallery-image").length-1){
            gallery.find(".big-gallery-next").hide();
        }else{
            gallery.find(".big-gallery-next").show();
        }
        if ($(window).width() > 480){
            gallery.find(".big-gallery-images").css({"left": -823*gal_ch});
        }else{
            gallery.find(".big-gallery-images").css({"left": -(gallery.find(".big-gallery-image").width()+15)*gal_ch});
        }
        gallery.find(".big-gallery-image").css({"opacity": 0.1});
        gallery.find(".big-gallery-image").eq(gal_ch).css({"opacity": 1});
        gallery.find(".big-gallery-num b").html("0"+(gal_ch+1));
    });
    var press_ok = false;
    var old_x = 0;
    var new_x = 0;
    $(document).on("touchstart", '.big-gallery-images', function(e){
        press_ok = true;
        old_x = e.originalEvent.touches[0].pageX;
        new_x = e.originalEvent.touches[0].pageX;
    });
    $(document).on("touchmove", '.big-gallery-images', function(e){
        if (press_ok){
            new_x = e.originalEvent.touches[0].pageX;
        }
    });
    $(document).on("touchend", '.big-gallery-images', function(e){
        if (press_ok){
            press_ok = false;
            if (Math.abs(old_x - new_x) > 100){
                if (old_x < new_x){
                    if (gal_ch > 0){
                        gallery.find(".big-gallery-image").eq(gal_ch-1).click();
                    }
                } else {
                    if (gal_ch < gallery.find(".big-gallery-image").length-1){
                        gallery.find(".big-gallery-next").click();
                    }
                }
            }
            old_x = 0;
            new_x = 0;
        }
    });

}
function createInfoGallery(gallery){
    var gal_ch = 0;
    gallery.find(".info-gallery-next").on("click", function () {
        gal_ch++;
        if (gal_ch >= gallery.find(".info-gallery-image").length-1){
            gallery.find(".info-gallery-next").hide();
        }else{
            gallery.find(".info-gallery-next").show();
        }
        if ($(window).width() > 480){
            gallery.find(".info-gallery-images").css({"left": -474*gal_ch});
        }else{
            gallery.find(".info-gallery-images").css({"left": -(gallery.find(".info-gallery-image").width()+15)*gal_ch});
        }
        gallery.find(".info-gallery-image").css({"opacity": 0.1});
        gallery.find(".info-gallery-image").eq(gal_ch).css({"opacity": 1});
        gallery.find(".info-gallery-num b").html("0"+(gal_ch+1));
    });
    gallery.find(".info-gallery-image").on("click", function () {
        gal_ch = $(this).index();
        if (gal_ch >= gallery.find(".info-gallery-image").length-1){
            gallery.find(".info-gallery-next").hide();
        }else{
            gallery.find(".info-gallery-next").show();
        }
        if ($(window).width() > 480){
            gallery.find(".info-gallery-images").css({"left": -474*gal_ch});
        }else{
            gallery.find(".info-gallery-images").css({"left": -(gallery.find(".info-gallery-image").width()+15)*gal_ch});
        }

        gallery.find(".info-gallery-image").css({"opacity": 0.1});
        gallery.find(".info-gallery-image").eq(gal_ch).css({"opacity": 1});
        gallery.find(".info-gallery-num b").html("0"+(gal_ch+1));
    });
    var press_ok = false;
    var old_x = 0;
    var new_x = 0;
    $(document).on("touchstart", '.info-gallery-images', function(e){
        press_ok = true;
        old_x = e.originalEvent.touches[0].pageX;
        new_x = e.originalEvent.touches[0].pageX;
    });
    $(document).on("touchmove", '.info-gallery-images', function(e){
        if (press_ok){
            new_x = e.originalEvent.touches[0].pageX;
        }
    });
    $(document).on("touchend", '.info-gallery-images', function(e){
        if (press_ok){
            press_ok = false;
            if (Math.abs(old_x - new_x) > 100){
                if (old_x < new_x){
                    if (gal_ch > 0){
                        gallery.find(".info-gallery-image").eq(gal_ch-1).click();
                    }
                } else {
                    if (gal_ch < gallery.find(".info-gallery-image").length-1){
                        gallery.find(".info-gallery-next").click();
                    }
                }
            }
            old_x = 0;
            new_x = 0;
        }
    });
}
