<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');


$product_id = intval($_POST["product_id"]);

if(CModule::IncludeModule("sale") && $product_id > 0)
{
    echo CSaleBasket::Delete($product_id);
}
?>