<div class="content">
    <div id="home-blog">
        <div id="home-blog-info">
            <a href="/blog/">
                <div class="home-blog-subtitle">блог</div>
            </a>
            <a href="/blog/">
                <div class="home-blog-title">Коротко<br>о коротких...</div>
            </a>
            <div class="home-blog-text">
                <a href="/blog/" class="home-blog-text-link">Салоны красоты, в которых лучше
                    всего делают короткие стрижки.
                    Куда идти за идеальным пикси,
                    каре и ежиком.</a><br>
                <a href="/blog/" class="show-more"><span>Узнать</span> больше</a>
            </div>
        </div>
    </div>
    <img src="<?=SITE_TEMPLATE_PATH.'/';?>img/main_blog.png" id="home-blog-img" alt="">
</div>