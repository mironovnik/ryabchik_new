<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Блог");
?>
<div id="all-content">

    <div id="page-top" class="white-top-content">
        <div class="top-cover" style="background: url(<?=SITE_TEMPLATE_PATH.'/img/main_blog.png'?>) no-repeat 50% 50%; background-size: cover;"></div>
        <div id="top-content">
            <div id="top-info">
                <div class="top-bookmark"><span>Блог</span></div>
                <div class="top-subtitle">eveRYday</div>
                <div class="top-title">Коротко о коротких... и не только</div>
                <div class="top-text">
                    Салоны красоты, в которых лучше всего делают короткие стрижки. Куда идти за идеальным пикси, каре и ежиком.<br>
                    <a href="/show-more/" class="show-more"><span>Узнать</span> больше</a>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div id="blog-list-title">Все публикации <span>2018</span> <a href="#">2017</a></div>
        <div class="list-bookmarks">
            <a href="#">eveRYday</a>
            <a href="#">Красота</a>
            <a href="#">Стиль</a>
        </div>
        <div class="blog-items">
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/1.png';?>" alt="">
                <small>eveRYday</small>
                <span>Что такое slow beauty и почему это тренд</span>
            </a>
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/2.png';?>" alt="">
                <small>Красота</small>
                <span>В концепт-сторе Aizel прошла вечеринка в честь презентации часов Chanel Code Coco</span>
            </a>
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/3.png';?>" alt="">
                <small>Стиль</small>
                <span>Какие вещи войдут в историю</span>
            </a>
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/4.png';?>" alt="">
                <small>eveRYday</small>
                <span>Что нужно знать о диете для чувствительной кожи</span>
            </a>
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/5.png';?>" alt="">
                <small>Стиль</small>
                <span>Костюмы и шелковые платья идеальных цветов в съемке бренда Nebo</span>
            </a>
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/6.png';?>" alt="">
                <small>Стиль</small>
                <span>Как создавалась дебютная коллекция Кима Джонса для Dior Homme</span>
            </a>
            <a href="/blog_open/" class="blog-item double-blog-size">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/7.png';?>" alt="">
                <small>eveRYday</small>
                <span>Loewe, мужская коллекция весна-лето 2019</span>
            </a>
            <a href="/blog_open/" class="blog-item">
                <img src="<?=SITE_TEMPLATE_PATH.'/img/blog/8.png';?>" alt="">
                <small>Стиль</small>
                <span>Мужские стрижки 2018: главные тенденции</span>
            </a>
        </div>
        <div class="load-more-link-block">
            <a href="#" class="load-more"></a>
        </div>
    </div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");

?>