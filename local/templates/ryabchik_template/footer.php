<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div id="footer">
    <div class="content">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "footer_menu",
            Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(""),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "footer-1",
                "USE_EXT" => "N"
            )
        );?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "footer_menu",
            Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(""),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "footer-2",
                "USE_EXT" => "N"
            )
        );?>
        <div class="footer-items">
            <b>Красивые<br>
                волосы<br>
                здесь</b>
        </div>
        <div class="footer-items">
            <a href="#" class="soc"></a>
            <a href="#" class="soc"></a>
            <a href="#" class="soc"></a>
            <a href="#" class="soc"></a>
        </div>
    </div>
    <div id="footer-copyright">
        <span>© RYABCHIK. 2018. Все права защищены.</span>
        <div id="footer-present">
            <img src="<?=SITE_TEMPLATE_PATH.'/'?>img/present.png" alt="">
            <b>Твой подарок</b><br>
            Подробности <a href="/catalog/">здесь</a>
        </div>
    </div>
</div>
</div>

<div id="right-cart" class="modal">

    <div id="right-cart-bg"></div>
    <div id="right-cart-block">
        <? $APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket.line",
            "modal_basket",
            array(
                "HIDE_ON_BASKET_PAGES" => "Y",
                "PATH_TO_AUTHORIZE" => "",
                "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                "PATH_TO_REGISTER" => SITE_DIR . "login/",
                "POSITION_FIXED" => "N",
                "SHOW_AUTHOR" => "N",
                "SHOW_EMPTY_VALUES" => "Y",
                "SHOW_NUM_PRODUCTS" => "Y",
                "SHOW_PERSONAL_LINK" => "Y",
                "SHOW_PRODUCTS" => "Y",
                "SHOW_REGISTRATION" => "N",
                "SHOW_TOTAL_PRICE" => "Y",
                "COMPONENT_TEMPLATE" => ".default",
                "SHOW_DELAY" => "N",
                "SHOW_NOTAVAIL" => "N",
                "SHOW_IMAGE" => "Y",
                "SHOW_PRICE" => "Y",
                "SHOW_SUMMARY" => "Y"
            ),
            false
        ); ?>
    </div>
</div syusty>
<div id="auth-modal" class="modal">
    <div id="auth-bg"></div>
    <a href="#" id="auth-logo"></a>
    <div id="auth-block">
        <div id="auth-title">Регистрация</div>
        <div id="auth-form-block">
            <div id="auth-social">
                <b>Войти через:</b>
                <a href="#"><img src="img/soc_fb.png" height="20" alt=""></a>
                <a href="#"><img src="img/soc_gp.png" height="18" alt=""></a>
                <a href="#"><img src="img/soc_tw.png" height="20" alt=""></a>
            </div>
            <div class="auth-form-block">
                <div class="input-block">
                    <span>Ваше имя</span>
                    <input type="text"
                           oninput="if (this.value != ''){$(this).parent().addClass('active')}else{$(this).parent().removeClass('active')}">
                </div>
                <div class="input-block">
                    <span>Ваш телефон</span>
                    <input type="text"
                           oninput="if (this.value != ''){$(this).parent().addClass('active')}else{$(this).parent().removeClass('active')}">
                </div>
                <div class="input-block">
                    <span>Ваш e-mail *</span>
                    <input type="text"
                           oninput="if (this.value != ''){$(this).parent().addClass('active')}else{$(this).parent().removeClass('active')}">
                </div>
                <div class="auth-buttons">
                    <a href="#">Зарегистрироваться</a>
                    <a href="#">Вход</a>
                </div>
                <div class="form-confirm">
                    Я согласен(а) с условиями пользовательского соглашения
                    и с условиями получения, хранения и использования
                    персональных данных.
                </div>
                <div class="auth-title">Подписка</div>
                <h3>Купить подписку на обучающие видео-материалы стилистов парикмахерской «Рябчик»</h3>
                <b>Стоимость подписки — 600 рублей<br>
                    Срок действия подписки — 30 дней</b>
                <div class="auth-buttons subscribe-btn">
                    <a href="#">Купить</a>
                </div>
            </div>
        </div>
        <div id="close-auth"></div>
    </div>
</div>
<div id="search-modal" class="modal">
    <div id="close-search"></div>
    <div class="serch-half"></div>
    <div id="search-form">
        <?$APPLICATION->IncludeComponent("bitrix:catalog.search", "search", Array(
            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CHECK_DATES" => "N",	// Искать только в активных по дате документах
            "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
            "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
            "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
            "DISPLAY_COMPARE" => "N",	// Выводить кнопку сравнения
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
            "ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
            "ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
            "ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
            "HIDE_NOT_AVAILABLE" => "N",	// Недоступные товары
            "HIDE_NOT_AVAILABLE_OFFERS" => "N",	// Недоступные торговые предложения
            "IBLOCK_ID" => "2",	// Инфоблок
            "IBLOCK_TYPE" => "CATALOG",	// Тип инфоблока
            "LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
            "NO_WORD_LOGIC" => "N",	// Отключить обработку слов как логических операторов
            "OFFERS_LIMIT" => "5",	// Максимальное количество предложений для показа (0 - все)
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_TITLE" => "Товары",	// Название категорий
            "PAGE_ELEMENT_COUNT" => "30",	// Количество элементов на странице
            "PRICE_CODE" => array(	// Тип цены
                0 => "BASE",
            ),
            "PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
            "PRODUCT_PROPERTIES" => "",	// Характеристики товара
            "PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
            "PROPERTY_CODE" => array(	// Свойства
                0 => "",
                1 => "",
            ),
            "RESTART" => "N",	// Искать без учета морфологии (при отсутствии результата поиска)
            "SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
            "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
            "SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
            "USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
            "USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
            "USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
        ),
            false
        );?>
    </div>
</div>

<? $APPLICATION->IncludeComponent("bitrix:news.list", "brands_list", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
    "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
    "AJAX_MODE" => "N",    // Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
    "CACHE_TYPE" => "A",    // Тип кеширования
    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
    "COMPONENT_TEMPLATE" => ".default",
    "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DISPLAY_DATE" => "Y",    // Выводить дату элемента
    "DISPLAY_NAME" => "Y",    // Выводить название элемента
    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "FIELD_CODE" => array(    // Поля
        0 => "NAME",
        1 => "",
    ),
    "FILTER_NAME" => "",    // Фильтр
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => "1",    // Код информационного блока
    "IBLOCK_TYPE" => "BRANDS",    // Тип информационного блока (используется только для проверки)
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
    "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
    "NEWS_COUNT" => "20",    // Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
    "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
    "PAGER_TITLE" => "Новости",    // Название категорий
    "PARENT_SECTION" => "",    // ID раздела
    "PARENT_SECTION_CODE" => "",    // Код раздела
    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
    "PROPERTY_CODE" => array(    // Свойства
        0 => "",
        1 => "",
    ),
    "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
    "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
    "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
    "SET_STATUS_404" => "N",    // Устанавливать статус 404
    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
    "SHOW_404" => "N",    // Показ специальной страницы
    "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
    "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
    "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
),
    false
); ?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "slide_catalog_menu", Array(
    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
    "IBLOCK_ID" => "2",	// Инфоблок
    "IBLOCK_TYPE" => "CATALOG",	// Тип инфоблока
    "SECTION_CODE" => "",	// Код раздела
    "SECTION_FIELDS" => array(	// Поля разделов
        0 => "",
        1 => "",
    ),
    "SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
    "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
    "SECTION_USER_FIELDS" => array(	// Свойства разделов
        0 => "",
        1 => "",
    ),
    "SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
    "TOP_DEPTH" => "4",	// Максимальная отображаемая глубина разделов
    "VIEW_MODE" => "LIST",	// Вид списка подразделов
    "COMPONENT_TEMPLATE" => ".default"
),
    false
);?>
</body>
</html>
