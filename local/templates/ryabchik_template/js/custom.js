function updateCartSumm() {
    var $new_summ = 0;
  $('.cart-remove-btn').each(function(index, element){
     $new_summ += $(element).data('price');

  });
    $('.total-sum').text($new_summ);
}

$(document).on('click', '.add-to-cart', function (e) {
//вызываем обработчик по клику на кнопку добавления
    e.preventDefault();
    var $id = $(this).data("id");
    $.ajax({
        url: "/php_scripts/add2basket/add2basket.php", // путь к php скрипту
        type: "POST",
        dataType: "json",
        data: {product_id: $id},
        success: function (basket_id) {
            alert('Товар добавлен в корзину');
            BX.onCustomEvent('OnBasketChange');
        },
    });
    $.ajax({
        url: "/php_scripts/update_count/update_count.php",
        type: "POST",
        success: function(count) {
            $('#cart-count').text(count);
        }
    });

});
$(document).on('click', '.cart-remove-btn', function (e) {
    e.preventDefault();
    var $id = $(this).data("id");
    $(this).parent().remove();
    $.ajax({
        url: "/php_scripts/deletefrombasket/deleteFromBasket.php", // путь к php скрипту
        type: "POST",
        dataType: "json",
        data: {product_id: $id},
        success: function (basket_id) {
            BX.onCustomEvent('OnBasketChange');
            updateCartSumm();
        },
    });
    $.ajax({
        url: "/php_scripts/update_count/update_count.php",
        type: "POST",
        success: function(count) {
            var count_products = 0;
            $('.cart-item').each(function(index,element) {
                count_products++;
            });
            console.log(count_products);
            if (count_products > 0){
                $('#cart-count').text(count_products);
                $('.basket-count-in-cart').text(count_products);
            }
            else {
                $('#cart-count').text(count_products);
                $('#cart-page-title').text('В корзине нет товаров');
                $('#cart-items').remove();
                $('#send-form').toggleClass('unactive');
                $('.basket-count-in-cart').toggleClass('unactive');
                $('.itog-summ').toggleClass('unactive');
            }

        }
    });

});
