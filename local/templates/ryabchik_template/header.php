<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;


?>
    <html lang="ru">
    <head>
        <? $APPLICATION->ShowHead() ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/fonts/fonts.css"); ?>
        <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css"); ?>
        <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/custom.css"); ?>
        <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-3.1.1.min.js"); ?>
        <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.mousewheel.min.js"); ?>
        <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/functions.js"); ?>
        <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/custom.js"); ?>
        <title><? $APPLICATION->ShowTitle(); ?></title>

    </head>
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
$arDir = explode('/', $dir);
$arDir = array_diff($arDir, array(''));
$productDir = $arDir[3];
$cartDir = $arDir[1];
if ($cartDir == 'cart') $isCart = true;
if ($productDir[0] == 'p' && $productDir[1] == '-') $isProduct = true;
?>
<body <?if ($isProduct) echo 'class="product-page"' ?>>
<? $APPLICATION->ShowPanel() ?>
    <div id="content-size"></div>

    <div id="fixed-left-menu">
        <a href="/" id="logo"></a>
        <a href="/cart/" id="mobile-cart">0</a>
        <div id="burger">
            <div class="burger-line"></div>
            <div class="burger-line"></div>
            <div class="burger-line"></div>
        </div>
        <div id="menu-items">
            <a href="#" class="catalog-menu-link"><b>Каталог</b></a>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "catalog-menu",
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "catalog_sections",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_TYPE" => "Y",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "ROOT_MENU_TYPE" => "catalog_sections",
                    "USE_EXT" => "Y",
                    "COMPONENT_TEMPLATE" => "catalog-menu"
                ),
                false
            ); ?>
            <a href="#" class="brands-menu-btn"><b>Бренды</b></a>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "left_head_menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "left-head",
                    "USE_EXT" => "N"
                )
            ); ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "mobile_menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "mobile",
                    "USE_EXT" => "N"
                )
            ); ?>
        </div>
    </div>

<div id="all-content">
<?
if ($isProduct || $isCart) echo '<div id="white-top" class="white-top-content">'
?>
<? if (!$isProduct && !$isCart) echo '<div id="page-top">' ?>
<? /* Если мы находимся на главной */ ?>
<? if ($APPLICATION->GetCurPage(false) === '/'): ?>
    <div class="top-cover"
         style="background: url(<?= SITE_TEMPLATE_PATH . '/img/main_img.png'; ?>) no-repeat 50% 50%; background-size: cover;"></div>
    <div id="top-content">
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "main_article",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", ""),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "4",
                "IBLOCK_TYPE" => "ARTICLES",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array("ARTICLE_NUMBER", ""),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N"
            )
        ); ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "additional_articles",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("", ""),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "5",
                "IBLOCK_TYPE" => "ARTICLES",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array("ARTICLE_NUMBER", ""),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N"
            )
        ); ?>
    </div>
<? endif; ?>
    <div id="top-right">
        <a href="#" id="auth-btn">Войти</a>
        <div id="search-btn"></div>
        <div id="wishlist-btn"></div>
        <a href="/cart/" id="cart-count"><?= getCountBasket() ?></a>
    </div>
<? if (!$isProduct && !$isCart) echo '</div>'; ?>