<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(false);
CModule::IncludeModule("iblock");
?>
<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();
$arDir = explode('/', $dir);
$arDir = array_diff($arDir, array(''));
$productDir = $arDir[3];
if ($productDir[0]=='p' && $productDir[1]=='-') $isProduct = true;
if(!$isProduct) echo '<div id="white-catalog-bg"></div>'
?>
<div id="catalog" class="content">
    <div id="catalog-title"><?
        $obj = new CIBlockSection;
        $res = $obj->GetByID($arResult['ORIGINAL_PARAMETERS']["SECTION_ID"]);
        if($arSection = $res->GetNext()) { ?> <?=$arSection['NAME'];?>
        <? } ?>
        <span>
            <?
            $iblock_data = new  CIBlockElement;
            $arFilter = Array("IBLOCK_ID"=>2, "SECTION_ID"=>$arResult['ORIGINAL_PARAMETERS']["SECTION_ID"], "INCLUDE_SUBSECTIONS" => "Y");
            $res_count = $iblock_data->GetList(Array(), $arFilter, Array(), false, Array());
            print_r($res_count);
            ?>
        </span></div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "subsect",
        array(
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COUNT_ELEMENTS" => "Y",
            "IBLOCK_ID" => "2",
            "IBLOCK_TYPE" => "CATALOG",
            "SECTION_CODE" => "",
            "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_ID" => $_REQUEST["SECTION_ID"],
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SHOW_PARENT_NAME" => "Y",
            "TOP_DEPTH" => "4",
            "VIEW_MODE" => "LIST",
            "COMPONENT_TEMPLATE" => "subsect"
        ),
        false
    );?>
    <div id="products">
        <?foreach($arResult["ITEMS"] as $arItem ):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
        <div class="product" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="<?=$arItem['NAME'];?>"></a>
            <div class="product-info">
                <?

                $res = $obj->GetByID($arItem["IBLOCK_SECTION_ID"]);

                if($arSection = $res->GetNext()) { ?>
                    <a href="<?=$arSection['SECTION_PAGE_URL'];?>">
                        <small>
                            <?=$arSection['NAME'];?>
                        </small>
                    </a>
                <? } ?>
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><b><?=$arItem['NAME'];?></b></a>
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><span><?=$arItem['PREVIEW_TEXT'];?></span></a>
                <div class="product-price">
                    <?=$arItem['PRICES']['BASE']['VALUE'];?> <small>о</small>
                    <div class="product-links">
                        <a href="#" class="add-to-cart" data-id="<?=$arItem['ID'];?>">В корзину</a>
                        <div class="product-links-border"></div>
                        <a href="#" class="add-to-wishlist"></a>
                    </div>
                </div>
            </div>
        </div>
        <?endforeach;?>
    </div>
</div>
