<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(false);

?>

<div id="home-bestsellers" class="content">
    <div id="bestsellers-title">
        Продукты-<br>
        бестселлеры
    </div>
    <div id="bestsellers-items">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
        <div class="product" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<?=$arItem['NAME'];?>"></a>
            <div class="product-info">
                <?
                $obj = new CIBlockSection;
                $res = $obj->GetByID($arItem["IBLOCK_SECTION_ID"]);

                if($arSection = $res->GetNext()) { ?>
                    <a href="<?=$arSection['SECTION_PAGE_URL'];?>">
                        <small>
                            <?=$arSection['NAME'];?>
                        </small>
                    </a>
                <? } ?>
                <a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><b><?=$arItem['NAME'];?></b></a>
                <a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><span><?=$arItem['PREVIEW_TEXT'];?></span></a>
                <div class="product-price">
                    <?=$arItem['PRICES']['BASE']['VALUE'];?>
                    <small>о</small>
                    <div class="product-links">
                        <a href="#" data-id="<?=$arItem['ID'];?>" class="add-to-cart">В корзину</a>
                        <div class="product-links-border"></div>
                        <a href="#" class="add-to-wishlist"></a>
                    </div>
                </div>
            </div>
        </div>
        <?endforeach;?>
    </div>
    <div class="bestsellers-next"></div>
</div>