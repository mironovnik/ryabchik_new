



<div id="result-items">
    <b>Результаты поиска <span><?=count($arResult['ITEMS']);?></span></b>
    <?foreach($arResult['ITEMS'] as $arItem):?>
    <a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="result-item">
        <div class="result-item-img"
             style="background: url(<?=$arItem['PREVIEW_PICTURE']['SRC'];?>) no-repeat 50% 50%; background-size: contain;"></div>
        <div class="result-item-text">
            <b><?=$arItem['NAME'];?></b>
        </div>
    </a>
    <?endforeach;?>
</div>
