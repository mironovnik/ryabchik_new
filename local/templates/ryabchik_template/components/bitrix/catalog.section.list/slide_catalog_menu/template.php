<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? foreach ($arResult['SECTIONS'] as &$arSection): ?>
    <?
    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
    ?>
    <? if ($arSection['DEPTH_LEVEL'] != 1) continue; ?>
    <div id="catalog-menu" class="catalog-menu">
        <div id="catalog-menu-items">
            <div id="close-catalog-menu" class="close-catalog-menu"></div>
            <div class="catalog-menu-items">
                <div class="catalog-menu-items-list">
                    <? if ($arSection['DEPTH_LEVEL'] == 1) $sectionUrlFirstLevel = $arSection['SECTION_PAGE_URL']; ?>
                    <? foreach ($arResult['SECTIONS'] as &$arSection): ?>
                        <? if ($arSection['DEPTH_LEVEL'] == 2 && stripos($arSection['SECTION_PAGE_URL'], $sectionUrlFirstLevel) !== false): ?>
                            <a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME']; ?></a>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
                <div class="catalog-menu-items-list">
                    <? foreach ($arResult['SECTIONS'] as &$arSection): ?>
                        <? if ($arSection['DEPTH_LEVEL'] == 3 && stripos($arSection['SECTION_PAGE_URL'], $sectionUrlFirstLevel) !== false) : ?>
                            <a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME']; ?></a>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>

            <div id="catalog-menu-products">
                <div id="catalog-menu-title">Шампунь <span>26</span></div>
                <a href="product.html" class="product-top-item">
                    <div class="product-top-item-img"
                         style="background: url(img/bran_product2.png) no-repeat 50% 50%; background-size: cover;"></div>
                    <b>Davines Essential Haircare Momo Haur Potion</b>
                    Универсальный несмываемый увлажняющий крем
                </a>
                <a href="product.html" class="product-top-item">
                    <div class="product-top-item-img"
                         style="background: url(img/bran_product3.png) no-repeat 50% 50%; background-size: cover;"></div>
                    <b>Davines Essential Haircare Momo Haur Potion</b>
                    Универсальный несмываемый увлажняющий крем
                </a>
                <a href="product.html" class="product-top-item">
                    <div class="product-top-item-img"
                         style="background: url(img/bran_product2.png) no-repeat 50% 50%; background-size: cover;"></div>
                    <b>Davines Essential Haircare Momo Haur Potion</b>
                    Универсальный несмываемый увлажняющий крем
                </a>
                <a href="product.html" class="product-top-item">
                    <div class="product-top-item-img"
                         style="background: url(img/bran_product3.png) no-repeat 50% 50%; background-size: cover;"></div>
                    <b>Davines Essential Haircare Momo Haur Potion</b>
                    Универсальный несмываемый увлажняющий крем
                </a>
                <a href="product.html" class="product-top-item">
                    <div class="product-top-item-img"
                         style="background: url(img/bran_product2.png) no-repeat 50% 50%; background-size: cover;"></div>
                    <b>Davines Essential Haircare Momo Haur Potion</b>
                    Универсальный несмываемый увлажняющий крем
                </a>
                <a href="product.html" class="product-top-item">
                    <div class="product-top-item-img"
                         style="background: url(img/bran_product3.png) no-repeat 50% 50%; background-size: cover;"></div>
                    <b>Davines Essential Haircare Momo Haur Potion</b>
                    Универсальный несмываемый увлажняющий крем
                </a>

            </div>
        </div>
        <div id="catalog-menu-right">

        </div>
    </div>
<?endforeach;?>