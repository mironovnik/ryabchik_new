<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?>
<div id="top-more-items">
    <?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <a href="#" class="top-more-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" width="113" alt="">
        <b><?=$arItem['PROPERTIES']['ARTICLE_NUMBER']['VALUE'];?></b>
        <span><?=$arItem['PREVIEW_TEXT'];?></span>
    </a>
    <?endforeach;?>
</div>