<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div id="top-info">
    <div class="top-subtitle">Рекомендуем</div>
    <?foreach ($arResult['ITEMS'] as $arItem):?>
    <div class="top-title">
        <div class="top-number half-number"><?=$arItem['PROPERTIES']['ARTICLE_NUMBER']['VALUE'];?></div>
        <?=$arItem['PREVIEW_TEXT'];?>
    </div>
    <div class="top-text">
        <?=$arItem['DETAIL_TEXT'];?><br>
        <a href="#" class="show-more"><span>Узнать</span> больше</a>
    </div>
    <?endforeach;?>
</div>