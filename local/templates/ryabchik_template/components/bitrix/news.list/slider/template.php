<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(false);
?>
<!--Слайдер через news list-->
<div id="product-brands">
    <div id="product-brands-list">
        <?foreach($arResult["ITEMS"] as $arItem ):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="product-brand" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
            <div class="product-brand-info">
                <?=$arItem['PREVIEW_TEXT'];?>
                <a href="/brands/" class="show-more"><span>Узнать</span> больше</a>
            </div>
        </div>
        <?endforeach;?>
    </div>
    <div class="product-brands-next"></div>

</div>