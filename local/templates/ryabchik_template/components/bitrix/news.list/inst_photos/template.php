<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="instagram-block">
    <div class="instagram-title">Beauty in real life <small>(on Instagram)</small></div>
    <div id="instagram-photos">
        <? $obj = new CFile; ?>
        <?foreach($arResult['ITEMS'] as $arItem ):?>
            <?foreach($arItem['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $arPhoto):?>
                <? $photo = $obj->GetPath($arPhoto);?>
                <img src="<?=$photo;?>" alt="">
            <?endforeach;?>
        <?endforeach;?>
    </div>
</div>
