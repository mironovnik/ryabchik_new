<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
\Bitrix\Main\UI\Extension::load("ui.fonts.ruble");

$documentRoot = Main\Application::getDocumentRoot();


$arParams['USE_PRICE_ANIMATION'] = isset($arParams['USE_PRICE_ANIMATION']) && $arParams['USE_PRICE_ANIMATION'] === 'N' ? 'N' : 'Y';
$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string)$arParams['EMPTY_BASKET_HINT_PATH'] : '/';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';


\CJSCore::Init(array('fx', 'popup', 'ajax'));
$this->addExternalJs($templateFolder.'/js/mustache.js');
$this->addExternalJs($templateFolder.'/js/action-pool.js');
$this->addExternalJs($templateFolder.'/js/filter.js');
$this->addExternalJs($templateFolder.'/js/component.js');

?>
<div class="content">
    <?if (getCountBasket() == 0) {?>
        <div id="cart-page-title">В корзине нет товаров</div>
        <div id="cart-itog">
            <div id="cart-itog-block">
                <a href="/" class="buy-more hover-link">Вернуться к покупкам</a>
            </div>
        </div>
    <?}
     else
    {?>
    <div id="cart-page-title">Корзина <span class="basket-count-in-cart"><?= $arResult['BASKET_ITEMS_COUNT'] ?></span></div>
    <div id="cart-items">
        <? foreach ($arResult['ITEMS']['AnDelCanBuy'] as $arItem): ?>
            <div class="cart-item active">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="cart-image"
                   style="background: url(<?= $arItem['PREVIEW_PICTURE_SRC']; ?>) no-repeat 50% 50%; background-size: contain;"></a>
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="cart-title">
                    <?= $arItem['NAME'] ?>
                </a>
                <div class="cart-count">
                    <?= $arItem['QUANTITY'] ?>
                </div>
                <div class="cart-price">
                    <?= $arItem['PRICE'] ?> <span>о</span>
                </div>
                <div class="cart-price full-price">
                    <?= $arItem['SUM_VALUE'] ?> <span>о</span>
                </div>
                <div class="cart-remove-btn" data-id="<?=$arItem['ID']?>" data-price="<?=$arItem['SUM_VALUE']?>">
                    <img src="<?= SITE_TEMPLATE_PATH . '/' ?>img/cart_remove.svg" alt="" data-entity="basket-item-delete">
                </div>
                <div class="cart-wishlist-btn"><img src="<?= SITE_TEMPLATE_PATH . '/' ?>img/cart_wishlist.svg" alt="">
                </div>
            </div>
        <? endforeach ?>
    </div>
    <div id="cart-itog">
        <div id="cart-itog-block">
            <div class="itog-summ">
                Итого<span class="total-sum"><?= $arResult['allSum']; ?></span>
                <small>о</small>
            </div>
            <br>
            <a href="/order/" id="send-form">Оформить заказ</a>
            <a href="/" class="buy-more hover-link">Продолжить покупки</a>
        </div>
    </div>
    <?}?>
</div>
