<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY'])))
{
?>
		<div id="right-cart-list" >
			<?foreach ($arResult["CATEGORIES"] as $category => $items): ?>
				<?foreach ($items as $v):?>
					<a href="<?=$v['DETAIL_PAGE_URL'];?>" class="right-cart-item">
                        <div class="right-cart-img" style="background: url(<?=$v["PICTURE_SRC"]?>) no-repeat 50% 50%; background-size: contain;"></div>
                        <div class="right-cart-item-text">
                            <b><?=$v["NAME"]?></b>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>Количество</td>
                                    <td><?=$v["QUANTITY"]?></td>
                                </tr>
                                <tr>
                                    <td>Стоимость</td>
                                    <td><?=$v["FULL_PRICE"]?><small>о</small></td>
                                </tr>
                                <tr>
                                    <td>Итого</td>
                                    <td><?=$v["SUM"]?> <small>о</small></td>
                                </tr>
                            </table>
                        </div>
					</a>
				<?endforeach?>
			<?endforeach?>
		</div>
        <div id="right-cart-title">Корзина <span><?=$arResult['NUM_PRODUCTS']?></span></div>
        <div id="right-cart-footer">
            <span>Итого <b><?=$arResult['TOTAL_PRICE']?></b><small>о</small></span>
            <div id="right-cart-buttons">
                <a href="/order/">Оформить заказ</a>
                <a href="/cart/">Перейти в корзину</a>
            </div>
        </div>
        <div id="close-cart"></div>
	<script>
		BX.ready(function(){
			<?=$cartId?>.fixCart();
		});
	</script>
<?
}
else {
    ?>
    <div id="right-cart-title">Ваша корзина пуста</div>
    <div id="close-cart"></div>
<?
}