<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");
?>

    <div id="all-content">

        <div class="content mobile-white">
            <div id="white-top">



                <div id="products-bookmarks" class="left-position"><span style="margin: 0">О нас</span></div>
                <div class="text-header">
                    <small>welcome</small>
                    <h1>Свет, талант,<br>
                        душа и любовь...</h1>
                    <div class="header-full-text">
                        Если ты ценишь простоту, но помнишь про уникальность;<br>
                        любишь стиль, но равнодушен к манерности. Одним словом,<br>
                        если ты человек европейского мышления, тогда эта<br>
                        информация для тебя!
                    </div>
                </div>
            </div>
        </div>

        <div class="content ">

            <div class="about-content">
                <p style="margin-top: 0">Парикмахерские «РЯБЧИК» — не beauty,  не spa,  не salon,  не hair style или что-то в этом пафосном духе и  часто не оправдывающем свое название.</p>
                <div class="about-big-text">
                    Рябчик — уникальные пространства на Покровке и Патриарших прудах, где всегда горит свет, где талант, душа и любовь к парикмахерскому делу сочетается с премиальным уровнем сервиса.
                </div>

            </div>
            <div class="big-gallery">
                <div class="big-gallery-images">
                    <div class="big-gallery-image" style="background: url(<?=SITE_TEMPLATE_PATH.'/img/about_photo1.png'?>) no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="big-gallery-image" style="background: url(<?=SITE_TEMPLATE_PATH.'/img/about_photo1.png'?>) no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="big-gallery-image" style="background: url(<?=SITE_TEMPLATE_PATH.'/img/about_photo1.png'?>) no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="big-gallery-image" style="background: url(<?=SITE_TEMPLATE_PATH.'/img/about_photo1.png'?>) no-repeat 50% 50%; background-size: cover;"></div>
                    <div class="big-gallery-image" style="background: url(<?=SITE_TEMPLATE_PATH.'/img/about_photo1.png'?>) no-repeat 50% 50%; background-size: cover;"></div>
                </div>
                <div class="big-gallery-next"></div>
                <div class="big-gallery-num"><b>01</b> / 05</div>
            </div>

            <div id="about-info">
                <div class="about-info">
                    <b>Адреса</b>
                    Покровка ул., 31,<br>
                    +7 985 330-31-31<br><br>

                    Малый Козихинский пер., 8/18,<br>
                    +7 985 330-32-32
                </div>
                <div class="about-info">
                    <b>Время работы</b>
                    Ежедневно с 10:00 до 21:00
                </div>
                <div class="about-info">
                    <b>Сотрудничество</b>
                    PR, СМИ<br>
                    Алёна Фамилия<br><br>

                    pr@ryabchik.net<br>
                    +7 968 778-08-77
                </div>
            </div>

            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "inst_photos",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "6",
                    "IBLOCK_TYPE" => "INST_PHOTOS",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "MORE_PHOTO",
                        2 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => "inst_photos"
                ),
                false
            ); ?>
        </div>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");

?>